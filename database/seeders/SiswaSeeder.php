<?php

use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            ['nama' => 'budi', 'nis' => '202220231', 'tgl_lahir' => '1998-08-12','alamat' => 'Bandung', 'jk' => 'L'],
            ['nama' => 'lala', 'nis' => '202220232', 'tgl_lahir' => '1995-05-16','alamat' => 'Jakarta', 'jk' => 'P']
        ];

        DB::table('siswas')->insert($sample);
    }
}
